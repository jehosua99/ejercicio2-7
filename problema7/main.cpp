#include <iostream>

using namespace std;
/*
 El usuario ingresara una palabra y el programa eliminará las letras repetidas aunque no distingue entre mayusculas y minusculas
*/

int main()
{
    int tamanio, repetidas = 0, posicion = 0;
    cout << "Ingrese la longitud de la palabra:\n";
    cin >> tamanio;
    char palabraingresada[tamanio], palabraSinRepetidas[25];  //el arreglo palabraSinRepetidas tiene un valor grande para que alcance una palabra sin letras repetidas
    cout << "Ingrese la palabra a la que se le eliminaran las letras repetidas:\n";
    cin >> palabraingresada;
    for (int i = 0; i <= tamanio; i ++)  //ciclo que recorrera caracter por caracter la palabra
    {
        for (int j = 0; j <= tamanio; j++)  //ciclo que recorrera la nueva palabra formada
        {
            if (palabraingresada[i] == palabraSinRepetidas[j])  //Condicional que verifica si la letra buscada ya se encuentra en la nueva palabra formada
                repetidas ++;  //le doy el nombre adecuado al contador
        }
        if (repetidas == 0)  //Si la letra no se encuentra en la nueva palabra el contador no cambiara de su valor inicial 0
        {
            palabraSinRepetidas[posicion] = {palabraingresada[i]};  //la letra que se estaba buscando (i) ingresara a la nueva palabra
            posicion ++;  // La posicion de la letra podra cambiar a la original por eso creo una nueva variable que aumentara de 1 en 1
        }
        repetidas = 0;
    }
    cout << "La palabra nueva es: " << palabraSinRepetidas <<endl;
    return 0;
}
